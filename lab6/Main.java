package IntroductionClass;

public class Main {

	public static void main(String[] args) {
		Point point1 = new Point(4,6);
		Rectangle rect = new Rectangle(5,9,point1);
		System.out.println(rect.area());
		System.out.println(rect.perimeter());
		Point[] corners = rect.corners();
		System.out.println("Coordinates:");
		for (int i = 0 ; i<corners.length; i++) {
			int x = corners[i].xCordinate;
			int y = corners[i].yCordinate;
			System.out.println("x:"+x+" "+"y:"+y);
		}
		Point point2 = new Point(2,3);
		Circle circle1 = new Circle(10, point2);
		Circle circle2 = new Circle(4,point1);
		System.out.println(circle1.area());
		System.out.println(circle1.perimeter());
		System.out.println(circle1.intersect(circle2));
	
	}
	
}
