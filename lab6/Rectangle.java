package IntroductionClass;

public class Rectangle {
	int sideA;
	int sideB;
	Point topLeft = new Point(sideA,sideB);
	
	public Rectangle(int a, int b, Point point) {
		this.sideA = a;
		this.sideB = b;
		this.topLeft = point;
	}
	
	public int area() {
		return (this.sideA*this.sideB);
	}
	
	public int perimeter() {
		return (2*(this.sideA+this.sideB));
	}
	
	public Point[] corners() {
		Point cornerarr[] = new Point[4];
		Point corner = this.topLeft;
		Point topRight = new Point((corner.xCordinate-sideA),corner.yCordinate);
		Point botRight = new Point((corner.xCordinate-sideA),(corner.yCordinate-sideB));
		Point botLeft = new Point(corner.xCordinate,(corner.yCordinate-sideB));
		cornerarr[0] = corner;
		cornerarr[1] = topRight;
		cornerarr[2] = botRight;
		cornerarr[3] = botLeft;
		return (cornerarr);
	}
}
