package IntroductionClass;

public class Circle {
	int radius;
	Point center;
	
	public Circle(int r , Point c) {
		this.radius = r;
		this.center = c;
	}
	
	public double area() {
		return Math.PI*(Math.pow(this.radius, 2));
	}
	
	public double perimeter() {
		return 2*Math.PI*this.radius;
	}
	
	public boolean intersect(Circle y) {
		boolean check = false;
		double centerline = Math.sqrt(Math.pow((this.center.xCordinate-y.center.xCordinate),2)+Math.pow(this.center.yCordinate-y.center.yCordinate, 2));
		double sumrad = this.radius+y.radius;
		if (centerline < sumrad) {
			check = true;
		}
		return check;
	}
	
	
}
