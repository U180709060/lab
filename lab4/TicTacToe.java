import java.io.IOException;
import java.util.Scanner;
public class TicTacToe {
    public static void main(String[] args) throws IOException {
        Scanner reader = new Scanner(System.in);
        char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };
        printBoard(board);
        int moves = 0;
        int row1;
        int col1;
        int row2;
        int col2;
        int winCheck=0;
        while(moves<9){
            do{
                System.out.print("Player 1 enter row number:");
                do{
                    row1 = reader.nextInt();
                    if(row1>3){
                        System.out.print("Try Again");
                    }
                }while(row1>3);
                System.out.print("Player 1 enter column number:");
                do{
                    col1 = reader.nextInt();
                    if(col1>3){
                        System.out.print("Try Again");
                    }
                }while(col1>3);
                if(board[row1 - 1][col1 - 1] != ' '){
                    System.out.println("Box is Full. Try Again");
                }
            }while(board[row1 - 1][col1 - 1] != ' ');
            board[row1 - 1][col1 - 1] = 'X';
            moves = moves+1;
            printBoard(board);
            if(checkBoard(board,row1,col1)){
                winCheck = 1;
                System.out.println("Congratulations, Player 1 won!");
                break;
            }
            if(moves==9){
                break;
            }
            do{
                System.out.print("Player 2 enter row number:");
                do{
                    row2 = reader.nextInt();
                    if(row2>3){
                        System.out.print("Try Again");
                    }
                }while(row2>3);
                System.out.print("Player 2 enter column number:");
                do{
                    col2 = reader.nextInt();
                    if(col2>3){
                        System.out.print("Try Again");
                    }
                }while(col2>3);

                if(board[row2 - 1][col2 - 1] != ' '){
                    System.out.println("Box is Full. Try Again.");
                }
            }while(board[row2 - 1][col2 - 1] != ' ');
            board[row2 - 1][col2 - 1] = 'O';
            moves = moves+1;
            printBoard(board);
            if(checkBoard(board,row2,col2)){
                winCheck = 1;
                System.out.println("Congratulations, Player 2 won!");
                break;
            }
        }
        if(winCheck!=1){
            System.out.println("TIE");
        }
        reader.close();
    }

    public static void printBoard(char[][] board) {
        System.out.println("    1   2   3");
        System.out.println("   -----------");
        for (int row = 0; row < 3; ++row) {
            System.out.print(row + 1 + " ");
            for (int col = 0; col < 3; ++col) {
                System.out.print("|");
                System.out.print(" " + board[row][col] + " ");
                if (col == 2){
                    System.out.print("|");
                }
            }
            System.out.println();
            System.out.println("   -----------");

        }
    }

    public static boolean checkBoard(char[][] board, int rowLast, int colLast)   {
        char symbol = board[rowLast-1][colLast-1];

        boolean win = true;
        for(int col = 0; col <3; col++) {
            if(board[rowLast - 1][col] != symbol) {
                win = false;
                break;
            }
        }

        if(win){
            return true;
        }

        win = true;
        for(int row = 0; row <3; row++) {
            if(board[row][colLast - 1] != symbol) {
                win = false;
                break;
            }
        }
        if(win){
            return true;
        }
        if(rowLast == colLast) {
            win = true;
            for (int i = 0; i < 3;i++ ){
                if(board[i][i] != symbol){
                    win = false;
                    break;
                }
            }
            if(win)
                return true;
        }
        if (rowLast + colLast == 2) {
            win = true;
            for (int i = 0; i < 3; i++) {
                if (board[2 - i][i] != symbol) {
                    win = false;
                    break;
                }
            }
        }
        if(win){
            return true;
        }
        return false;
    }
}
