public class GCDLoop{
	public static void main(String[] args) {
		int x = Integer.parseInt(args[0]);
		int y = Integer.parseInt(args[1]);
        int result = gcd(x,y);
        System.out.println(result);
  }
	public static int gcd(int a, int b) {
        int r = 0;
        while(b!=0){
            r = a % b;
            a = b;
            b = r;    
    }
        return a;
  }
}
